import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Cyspace',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Home'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _counter = 0;
  var textController = new TextEditingController();
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: Colors.white,
      appBar: AppBar(
        elevation: 0.0,
        backgroundColor: Colors.indigo,
        title: _searchField(),
        leading: IconButton(
          icon: Icon(Icons.menu,
            color: Colors.white,),
          onPressed: (){
            _scaffoldKey.currentState.openDrawer();
          },
        ),
        actions: [
          IconButton(icon: Icon(Icons.notifications_none,
             color: Colors.white,),
              onPressed: (){})
        ],
        centerTitle: true,
      ),
      drawer: drawer(),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Center(
              child: Container(
                padding: EdgeInsets.all(10),
               height: 200,
               child: Card(
                 elevation: 10.0,
                 child: Image.asset('images/furniture.jpg',
                  height: 200,
                  fit: BoxFit.fill,),
               ),
              ),
            ),
            SizedBox(height: 20.0,),
            Padding(
              padding: const EdgeInsets.only(left: 10.0,right: 10.0),
              child: MaterialButton(
                color: Colors.blue,
                minWidth: double.infinity,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(5.0),
                ),
                child: Text("Istatant Service Order",
                  style: TextStyle(color: Colors.white),),
                onPressed: ()
                {

                },
              ),
            ),
            SizedBox(height: 20.0,),
            Padding(
              padding: const EdgeInsets.only(left: 10.0,right: 10.0),
              child: MaterialButton(
                color: Colors.blue,
                minWidth: double.infinity,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(5.0),
                ),
                child: Text("Need Transport",
                  style: TextStyle(color: Colors.white),),
                onPressed: ()
                {

                },
              ),
            ),
            SizedBox(height: 20.0,),
            Padding(
              padding: const EdgeInsets.only(left: 10.0),
              child: Container(
                child: Text('Industrial Plants, Machineries',
                    style: TextStyle(
                      fontSize: 18.0,
                      fontWeight: FontWeight.bold,
                    )),
              ),
            ),
            Container(
                height: 250.0,
                child: ListView(
                  scrollDirection: Axis.horizontal,
                  children: <Widget>[
                    getCard('images/water_purifier.jpg', 'Water Purifier',"Ro Treatment plant","Water lonize machine"),
                    getCard('images/submersible-pump.png', 'Submericile Pump',"Borewell Submersible","CRI Submersible Pump"),
                    getCard('images/air_compressor.jpg', 'Air Compressor',"Ro Treatment plant","Water lonize machine"),
                    SizedBox(width: 20.0,)
                  ],
                )),
            SizedBox(height: 20.0,),
            Padding(
              padding: const EdgeInsets.only(left: 10.0),
              child: Container(
                child: Text('Industrial and Engineering products',
                    style: TextStyle(
                      fontSize: 18.0,
                      fontWeight: FontWeight.bold,
                    )),
              ),
            ),
            Container(
                height: 250.0,
                child: ListView(
                  scrollDirection: Axis.horizontal,
                  children: <Widget>[
                    getCard('images/water_purifier.jpg', 'Water Purifier',"Ro Treatment plant","Water lonize machine"),
                    getCard('images/automotive-oils.png', 'Automotive Oils',"Engine Oils","Lubricant oils"),
                    getCard('images/geyser.jpg', 'Water Heater & Geyser',"Solar water heatre","Electrinc Water geyser"),
                    SizedBox(width: 20.0,)
                  ],
                )),
            SizedBox(height: 20.0,),
            Padding(
              padding: const EdgeInsets.only(left: 10.0),
              child: Container(
                child: Text('Industrial and Engineering products',
                    style: TextStyle(
                      fontSize: 18.0,
                      fontWeight: FontWeight.bold,
                    )),
              ),
            ),
            Container(
                height: 250.0,
                child: ListView(
                  scrollDirection: Axis.horizontal,
                  children: <Widget>[
                    getCard('images/water_purifier.jpg', 'Water Purifier',"Ro Treatment plant","Water lonize machine"),
                    getCard('images/water_purifier.jpg', 'Water Purifier',"Ro Treatment plant","Water lonize machine"),
                    getCard('images/water_purifier.jpg', 'Water Purifier',"Ro Treatment plant","Water lonize machine"),
                    getCard('images/water_purifier.jpg', 'Water Purifier',"Ro Treatment plant","Water lonize machine"),
                    SizedBox(width: 20.0,)
                  ],
                )),
            SizedBox(height: 20.0,),
            footer(),
          ],
        ),
      ),
    );
  }

 Widget _searchField()
 {
   return Container(
     decoration: BoxDecoration(
       color: Colors.white,
       borderRadius: BorderRadius.all(Radius.circular(5.0)),
     ),
     height: 40.0,
     child: Row(
       children: [
         Padding(
           padding: EdgeInsets.fromLTRB(10, 10, 5, 5),
           child: Icon(Icons.search, color: Colors.black,),
         ),
         Expanded(
           child: TextField(
             controller: textController,
             decoration: InputDecoration(
                 border: InputBorder.none,
                 focusedBorder: InputBorder.none,
                 enabledBorder: InputBorder.none,
                 errorBorder: InputBorder.none,
                 disabledBorder: InputBorder.none,
                 hintStyle: TextStyle(color: Colors.grey, height: 2),
                 labelStyle: TextStyle(color: Colors.black),
                 contentPadding: EdgeInsets.fromLTRB(5, 0, 5, 8),
                 hintText: " Search Of Products"),
           ),
         ),
       ],
     ),
   );
 }

  Widget drawer()
  {
    return Drawer(
      child: Column(
        children: [
           SafeArea(
             minimum: EdgeInsets.only(top: 20.0),
             child: ListTile(
                 leading: CircleAvatar(
                  child: ClipOval(
                   child:  Image.asset("images/user.png",
                       fit: BoxFit.fill, width: 70, height: 70),
                     ),
                   backgroundColor: Colors.white),
                 trailing: Icon(Icons.supervised_user_circle_rounded,
                 color: Colors.indigo,),
                  title: Text("S.Vasathakumar ", style: TextStyle(color: Colors.black),),
                  subtitle: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text("vksusi007@gmail.com ", style: TextStyle(color: Colors.black),),
                      Text("9962437567", style: TextStyle(color: Colors.black),),
                ],
              ),
          ),
           ),
          SizedBox(height: 15.0,),
          Divider(),
          SizedBox(height: 15.0,),
          Container(
            child: Padding(
              padding: const EdgeInsets.only(left: 20.0),
              child: Column(
               children: [
                 Row(
                   children: [
                     Icon(Icons.home),
                     SizedBox(width: 10.0,),
                     Text("Home",style: TextStyle(color: Colors.black),),
                   ],
                 ),
                 SizedBox(height: 15.0,),
                 Row(
                   children: [
                     Icon(Icons.notifications),
                     SizedBox(width: 10.0,),
                     Text("notification",style: TextStyle(color: Colors.black),),
                   ],
                 ),
                 SizedBox(height: 15.0,),
                 Row(
                   children: [
                     Icon(Icons.wallet_travel),
                     SizedBox(width: 10.0,),
                     Text("My Wallet",style: TextStyle(color: Colors.black),),
                   ],
                 ),
                 SizedBox(height: 15.0,),
                 Row(
                   children: [
                     Icon(Icons.star_border_outlined),
                     SizedBox(width: 10.0,),
                     Text("My Orders",style: TextStyle(color: Colors.black),),
                   ],
                 ),
                 SizedBox(height: 15.0,),
                 Row(
                   children: [
                     Icon(Icons.email),
                     SizedBox(width: 10.0,),
                     Text("Transport Service",style: TextStyle(color: Colors.black),),
                   ],
                 ),
                 SizedBox(height: 15.0,),
                 Row(
                   children: [
                     Icon(Icons.transit_enterexit),
                     SizedBox(width: 10.0,),
                     Text("Transit Service",style: TextStyle(color: Colors.black),),
                   ],
                 ),
                 SizedBox(height: 15.0,),
                 Row(
                   children: [
                     Icon(Icons.quick_contacts_mail),
                     SizedBox(width: 10.0,),
                     Text("Quick Inspection",style: TextStyle(color: Colors.black),),
                   ],
                 ),
                 SizedBox(height: 15.0,),
                 Row(
                   children: [
                     Icon(Icons.support_agent_rounded),
                     SizedBox(width: 10.0,),
                     Text("Insurace Agent",style: TextStyle(color: Colors.black),),
                   ],
                 ),
                 SizedBox(height: 15.0,),
                 Row(
                   children: [
                     Icon(Icons.home_repair_service),
                     SizedBox(width: 10.0,),
                     Text("Service Engieer",style: TextStyle(color: Colors.black),),
                   ],
                 ),
                 SizedBox(height: 15.0,),
                 Row(
                   children: [
                     Icon(Icons.train),
                     SizedBox(width: 10.0,),
                     Text("Transport Agent",style: TextStyle(color: Colors.black),),
                   ],
                 ),
                 SizedBox(height: 15.0,),
                 Row(
                   children: [
                     Icon(Icons.support_agent),
                     SizedBox(width: 10.0,),
                     Text("Insurace Agent",style: TextStyle(color: Colors.black),),
                   ],
                 ),
                 SizedBox(height: 15.0,),
                 Row(
                   children: [
                     Icon(Icons.settings),
                     SizedBox(width: 10.0,),
                     Text("Settings",style: TextStyle(color: Colors.black),),
                   ],
                 ),
                 SizedBox(height: 15.0,),
                 Row(
                   children: [
                     Icon(Icons.help),
                     SizedBox(width: 10.0,),
                     Text("Help",style: TextStyle(color: Colors.black),),
                   ],
                 ),
                 SizedBox(height: 15.0,),
                 Row(
                   children: [
                     Icon(Icons.chat),
                     SizedBox(width: 10.0,),
                     Text("Chat",style: TextStyle(color: Colors.black),),
                   ],
                 ),
                 SizedBox(height: 15.0,),
                 Row(
                   children: [
                     Icon(Icons.share),
                     SizedBox(width: 10.0,),
                     Text("Share",style: TextStyle(color: Colors.black),),
                   ],
                 ),
                 SizedBox(height: 15.0,),
                 Row(
                   children: [
                     Icon(Icons.logout),
                     SizedBox(width: 10.0,),
                     Text("Logout",style: TextStyle(color: Colors.black),),
                   ],
                 ),
               ],
              ),
            ),
          ),

        ],
      ),
    );
  }

  Widget getCard(String imgPath, String serviceName, String subTitle, String secondSubTitle) {
    return Padding(
      padding: EdgeInsets.only(left: 15.0, top: 25.0, bottom: 10.0),
      child: Container(
          height: 200.0,
          width: 180.0,
          decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(12.0),
              boxShadow: [
                BoxShadow(
                    blurRadius: 3.0,
                    color: Colors.grey.withOpacity(0.3),
                    spreadRadius: 3.0
                )
              ]
          ),
          child: Column(
              children: <Widget>[
                Stack(
                  children: <Widget>[
                    Container(
                      height: 115.0,
                      width: 200.0,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.only(topLeft: Radius.circular(12.0), topRight: Radius.circular(12.0)),
                          image: DecorationImage(
                              image: AssetImage(imgPath),
                              fit: BoxFit.cover
                          )
                      ),
                    ),
                  ],
                ),
                SizedBox(height: 5.0),
                Padding(
                  padding: const EdgeInsets.only(left:15.0),
                  child: Text(serviceName,
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 14.0
                    ),
                  ),
                ),
                SizedBox(height: 5.0),
                Padding(
                  padding: const EdgeInsets.only(left:15.0),
                  child: Column(
                    children: <Widget>[
                      Text(subTitle,
                        style: TextStyle(
                            fontSize: 10.0,
                            color: Colors.grey
                        ),
                      ),
                      SizedBox(height: 5.0),
                      Text( secondSubTitle,
                        style: TextStyle(
                            fontSize: 10.0,
                            color: Colors.grey.withOpacity(0.5)
                        ),
                      ),
                      SizedBox(height: 5.0),
                      Container(
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.all(Radius.circular(5.0)),
                            border: Border.all(color: Colors.blue),
                            color: Colors.white
                        ),
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Text("View All", style: TextStyle(color: Colors.blue),),
                        ),
                      ),

                    ],
                  ),
                ),
                SizedBox(height: 5.0),
              ]
          )
      ),
    );
  }

 Widget footer()
 {
   return Container(
     padding: EdgeInsets.only(left: 10.0,right: 10.0),
     child: Column(
       crossAxisAlignment: CrossAxisAlignment.start,
       children: [
         Text("Our Partners", style: TextStyle(
           fontSize: 18.0,
           fontWeight: FontWeight.bold,
         )),
         Padding(
           padding: const EdgeInsets.only(top: 10.0),
           child: Row(
             mainAxisAlignment: MainAxisAlignment.spaceBetween,
             children: [
               Container(
                   height: 70.0,
                   width: 100.0,
                   child: Image.network('https://picsum.photos/250?image=9',
                     fit: BoxFit.cover,
                   )),
               Container(
                   height: 70.0,
                   width: 100.0,
                   child: Image.network('https://picsum.photos/250?image=9',
                     fit: BoxFit.cover,
                   )),

               Container(
                   height: 70.0,
                   width: 100.0,
                   child: Image.network('https://picsum.photos/250?image=9',
                     fit: BoxFit.cover,

                   )),
             ],
           ),
         ),
       ],
     ),
   );
 }
}
